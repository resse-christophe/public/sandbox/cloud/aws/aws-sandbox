package org.ressec.sandbox.cloud.aws.sqs.fifo.controller;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
public class FifoMessageController
{
    private long MESSAGE_DEDUPLICATION_ID = 1;

    /**
     * Gson response.
     */
    private JsonObject json = new JsonObject();
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();

    /**
     * Messages.
     */
    private List<Message> messages;

    /**
     * Amazon SQS client.
     */
    private AmazonSQS awsServiceSQS = AmazonSQSClientBuilder.defaultClient();


    /**
     * Sends a message.
     * @param message Message content to send.
     * @param response Response.
     */
    @GetMapping(path = "/sqs/fifo/send", produces = MediaType.APPLICATION_JSON_VALUE)
    public String send(@RequestParam("url") @NonNull String queueUrl, @RequestParam("groupId") @NonNull String groupId, @RequestParam("message") @NonNull String message, HttpServletResponse response)
    {
        if (groupId.isEmpty())
        {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "GroupId cannot be NULL nor empty!");
        }

        if (queueUrl.isEmpty())
        {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Queue URL must be provided!");
        }

        if (message.isEmpty())
        {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Message content cannot be NULL nor empty!");
        }

        AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();

        SendMessageRequest request = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(message)
                .withMessageGroupId(groupId)
                .withMessageDeduplicationId(Long.toString(MESSAGE_DEDUPLICATION_ID++));

        SendMessageResult result = sqs.sendMessage(request);

        json.addProperty("message", String.format("Message with id: '%s' and sequence: '%s' sent successfully to queue url: %s", result.getMessageId(), result.getSequenceNumber(), queueUrl));
        return new Gson().toJson(json);
    }

    /**
     * Reads messages.
     * @param response Response.
     */
    @GetMapping(path = "/sqs/fifo/read", produces = MediaType.APPLICATION_JSON_VALUE)
    public String read(@RequestParam("url") @NonNull String queueUrl, HttpServletResponse response)
    {
        AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();

        final ReceiveMessageRequest request = new ReceiveMessageRequest()
                .withQueueUrl(queueUrl)
                .withMaxNumberOfMessages(10);

        messages = sqs.receiveMessage(request).getMessages();

        return new Gson().toJson(messages);
    }
}

