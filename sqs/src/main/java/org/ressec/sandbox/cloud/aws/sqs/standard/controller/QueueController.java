package org.ressec.sandbox.cloud.aws.sqs.standard.controller;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.AmazonSQSException;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.ListQueuesResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
public class QueueController
{
    /**
     * Json response.
     */
    private JsonObject json = new JsonObject();
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();

    /**
     * Amazon SQS client.
     */
    private AmazonSQS awsServiceSQS = AmazonSQSClientBuilder.defaultClient();


    /**
     * Creates a SQS queue.
     * @param name Queue nqme.
     * @param response Response.
     */
    @GetMapping(path = "/sqs/queue/create", params = "name", produces = MediaType.APPLICATION_JSON_VALUE)
    public String create(@RequestParam("name") @NonNull String name, HttpServletResponse response)
    {
        String message;

        if (name.isEmpty())
        {
            throw new ResponseStatusException(HttpStatus.SEE_OTHER, "Queue name cannot be NULL or empty!");
        }

        CreateQueueRequest request = new CreateQueueRequest(name)
                .addAttributesEntry("DelaySeconds", "60")
                .addAttributesEntry("MessageRetentionPeriod", "86400");

        try
        {
            awsServiceSQS.createQueue(request);
        }
        catch (AmazonSQSException e)
        {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, String.format("Cannot create SQS queue name: '%s' due to: ", name), e);
        }

        message = String.format("SQS queue: %s created successfully", name);
        json.addProperty("message", message);
        return new Gson().toJson(json);
    }

    /**
     * List SQS queues.
     * @param response Response.
     */
    @GetMapping(path = "/sqs/queue/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public String list(HttpServletResponse response)
    {
        List<String> queues;

        ListQueuesResult result = awsServiceSQS.listQueues();
        queues = new ArrayList<>(result.getQueueUrls());

        return new Gson().toJson(queues);
    }

    /**
     * Deletes a queue given its name.
     * @param response Response.
     */
    @GetMapping(path = "/sqs/queue/delete", params = "url", produces = MediaType.APPLICATION_JSON_VALUE)
    public String delete(@RequestParam("url") @NonNull String url, HttpServletResponse response)
    {
        awsServiceSQS.deleteQueue(url);
        String message = String.format("SQS queue url: %s deleted successfully", url);
        json.addProperty("message", message);
        return new Gson().toJson(json);
    }
}

