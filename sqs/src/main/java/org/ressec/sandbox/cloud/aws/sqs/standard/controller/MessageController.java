package org.ressec.sandbox.cloud.aws.sqs.standard.controller;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
public class MessageController
{
    /**
     * AWS SQS default queue url.
     */
    private static final String DEFAULT_QUEUE_URL = "https://sqs.eu-west-3.amazonaws.com/348313306066/hemajoo-food-sqs-test";

    /**
     * Gson response.
     */
    private JsonObject json = new JsonObject();
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();

    /**
     * Messages.
     */
    private List<Message> messages;

    /**
     * Amazon SQS client.
     */
    private AmazonSQS awsServiceSQS = AmazonSQSClientBuilder.defaultClient();


    /**
     * Sends a message.
     * @param message Message content to send.
     * @param response Response.
     */
    @GetMapping(path = "/sqs/message/send", produces = MediaType.APPLICATION_JSON_VALUE)
    public String send(@RequestParam("queue") @NonNull String queueUrl, @RequestParam("message") @NonNull String message, HttpServletResponse response)
    {
        if (queueUrl.isEmpty())
        {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Queue URL must be provided!");
        }

        if (message.isEmpty())
        {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Message content cannot be NULL nor empty!");
        }

        SendMessageRequest request = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(message)
                .withDelaySeconds(5);
        awsServiceSQS.sendMessage(request);

        json.addProperty("message", String.format("Message sent successfully to queue url: %s", queueUrl));
        return new Gson().toJson(json);
    }

    /**
     * Reads messages.
     * @param response Response.
     */
    @GetMapping(path = "/sqs/message/read", produces = MediaType.APPLICATION_JSON_VALUE)
    public String read(@RequestParam("url") @NonNull String queueUrl, HttpServletResponse response)
    {
        AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();

        final ReceiveMessageRequest request = new ReceiveMessageRequest(queueUrl);

        messages = sqs.receiveMessage(request).getMessages();
        return new Gson().toJson(messages);
    }

    /**
     * Deletes a message.
     * @param index Index of the message to delete.
     * @param response Response.
     */
    @GetMapping(path = "/sqs/message/delete", params = "index", produces = MediaType.APPLICATION_JSON_VALUE)
    public String send(@RequestParam("index") Integer index, HttpServletResponse response)
    {
        AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();
        String message;

        if (index == null)
        {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Message index cannot be NULL!");
        }

        sqs.deleteMessage(DEFAULT_QUEUE_URL, messages.get(index).getReceiptHandle());

        message = String.format("Message at index: %s deleted successfully", index);
        json.addProperty("message", message);
        return new Gson().toJson(json);
    }
}

