package org.ressec.sandbox.cloud.aws.sqs.fifo.controller;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
public class FifoQueueController
{
    /**
     * Json response.
     */
    private JsonObject json = new JsonObject();
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();

    /**
     * Amazon SQS client.
     */
    private AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();


    /**
     * Creates a SQS FIFO queue.
     * @param name Queue name.
     * @param response Response.
     */
    @GetMapping(path = "/sqs/fifo/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public String create(@RequestParam("name") @NonNull String name, HttpServletResponse response)
    {
        final Map<String, String> attributes = new HashMap<>();

        if (name.isEmpty())
        {
            throw new ResponseStatusException(HttpStatus.SEE_OTHER, "FIFO queue name cannot be NULL or empty!");
        }

        if (!name.endsWith(".fifo"))
        {
            name += ".fifo";
        }

        // A FIFO queue must have the FifoQueue attribute set to true.
        attributes.put("FifoQueue", "true");
        // If the user doesn't provide a MessageDeduplicationId, generate a MessageDeduplicationId based on the content.
        attributes.put("ContentBasedDeduplication", "true");

        // The FIFO queue name must end with the .fifo suffix.
        final CreateQueueRequest createQueueRequest = new CreateQueueRequest(name).withAttributes(attributes);
        final String queueUrl = sqs.createQueue(createQueueRequest).getQueueUrl();

        json.addProperty("message", String.format("FIFO queue: %s created successfully with URL: %s", name, queueUrl));
        return new Gson().toJson(json);
    }
}

