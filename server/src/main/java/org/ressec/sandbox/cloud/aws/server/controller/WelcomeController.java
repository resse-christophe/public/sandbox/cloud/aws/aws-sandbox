package org.ressec.sandbox.cloud.aws.server.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController
{
    //@RequestMapping(value = "/welcome", method = RequestMethod.GET)
    @GetMapping(value = "/welcome")
    public String welcomeText()
    {
        return "AWS Sandbox Application\n";
    }

    //@RequestMapping(path = "/json/welcome", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping(path = "/json/welcome", produces = MediaType.APPLICATION_JSON_VALUE)
    public String welcomeJson()
    {
        String message = "AWS SandboxApplication - Copyright Resse Christophe (c) 2020\n";

        JsonObject element = new JsonObject();
        element.addProperty("message", message);

        return new Gson().toJson(element);
    }
}

