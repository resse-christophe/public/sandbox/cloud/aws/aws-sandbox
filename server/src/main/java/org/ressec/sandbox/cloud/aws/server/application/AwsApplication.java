/*
 * Copyright(c) 2019 Hemajoo Systems Inc.
 * --------------------------------------------------------------------------------------
 * This file is part of Hemajoo Systems Inc. projects which is licensed
 * under the Apache license version 2 and use is subject to license terms.
 * You should have received a copy of the license with the project's artifact
 * binaries and/or sources.
 *
 * License can be consulted at http://www.apache.org/licenses/LICENSE-2.0
 * --------------------------------------------------------------------------------------
 */
package org.ressec.sandbox.cloud.aws.server.application;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;

@SpringBootApplication
@ComponentScan("org.ressec.sandbox.cloud.aws")
public class AwsApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(AwsApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx)
    {
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot: ");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);

            for (String beanName : beanNames)
            {
                System.out.println(" -> " + beanName);
            }

            //initialize();
        };
    }

//    private void initialize()
//    {
//        Country country = null;
//
//        country = Country.builder()
//                .numeric(250)
//                .iso2("FR")
//                .iso3("FRA")
//                .name("France")
//                .official("The French Republic")
//                .build();
//
//        country = Country.builder()
//                .numeric(276)
//                .iso2("DE")
//                .iso3("DEU")
//                .name("Germany")
//                .official("The Federal Republic of Germany")
//                .build();
//
//        country = Country.builder()
//                .numeric(8)
//                .iso2("AL")
//                .iso3("ALB")
//                .name("Albania")
//                .official("The Republic of Albania")
//                .build();
//    }
}
